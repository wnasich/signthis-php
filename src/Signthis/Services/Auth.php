<?php

namespace Signthis\Services;

use Signthis\Client;
use Signthis\Service;

class Auth extends Service
{
    public function login($email, $password)
    {
        $data = array('user' => array(
            'email' => $email,
            'password' => $password,
        ));

        return $this->client->request('sign_in.json', 'POST', $data, 200);
    }

    public function logout()
    {
        return $this->client->request('sign_out.json', 'DELETE');
    }
}
