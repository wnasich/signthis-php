<?php

namespace Signthis\Services;

use Signthis\Client;
use Signthis\Service;

class Documents extends Service
{
    public function all()
    {
        return $this->client->request('documents.json', 'GET');
    }

    public function find($documentId)
    {
        return $this->client->request("documents/$documentId.json", 'GET');
    }

    public function create(array $signers, $emailBody, $documentName, $fileData)
    {
        $data = array('document' => array(
            'document_name' => $documentName,
            'document' => 'data:application/pdf;base64,' . base64_encode($fileData),
            'email_body' => $emailBody,
            'signers_attributes' => $signers,
        ));

        return $this->client->request('documents.json', 'POST', $data, 201);
    }

    public function cancel($documentId)
    {
        return $this->client->request("documents/$documentId/cancel.json", 'DELETE', array(), 204);
    }

    public function addDragSigner($documentId, $signerId, $page, $top, $left)
    {
        $data = array('drag_signer' => array(
            'page' => $page,
            'top' => $top,
            'left' => $left,
        ));

        return $this->client->request("documents/$documentId/signers/$signerId/drag_signers.json", 'POST', $data, 201);
    }

    public function donePlaceholders($documentId)
    {
        return $this->client->request("documents/$documentId/done_placeholders.json", 'PUT', array(), 204);
    }

    public function send($documentId, $isPaymentWithSignature = false)
    {
        $data = array('payment' => array(
            'is_payment_with_signature' => 'false',
        ));

        return $this->client->request("documents/$documentId/finish_template.json", 'PUT', $data, 204);
    }
}
