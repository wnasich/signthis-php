<?php

namespace Signthis;

abstract class ClientBase
{
    protected $url = 'https://signthis.ca/api/v1/';
    protected $apiKey = null;
    protected $timeout = 240;

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    protected function doRequest($url, $method, $data, $contentType)
    {
        $c = curl_init();

        $header = array(
            'Content-Type: ' . $contentType,
        );

        if ($this->apiKey)
        {
            $data['api_key'] = $this->apiKey;
        }

        $url = $this->url . $url;

        switch($method)
        {
            case 'GET':
                curl_setopt($c, CURLOPT_HTTPGET, true);
                $url .= '?' . http_build_query($data);
                break;

            case 'POST':
                curl_setopt($c, CURLOPT_POST, true);
                curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($data));
                break;

            case 'PUT':
                curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
                curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($data));
                break;

            case 'DELETE':
                curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
                $url .= '?' . http_build_query($data);
                break;
        }

        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_USERAGENT, 'Signthis/PHP');
        curl_setopt($c, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($c, CURLOPT_HEADER, true);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, $header);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($c);

        curl_close($c);

        return $response;
    }

    public function request($url, $method, $data = array(), $expectedHttpCode = 200, $contentType = 'application/json')
    {
        $response = $this->doRequest($url, $method, $data, $contentType);
        return $this->parseResponse($url, $response, $expectedHttpCode);
    }

    public function parseResponse($url, $response, $expectedHttpCode)
    {
        $header = false;
        $content = array();
        $status = 200;

        foreach(explode("\r\n", $response) as $line)
        {
            if (strpos($line, 'HTTP/1.1') === 0)
            {
                $lineParts = explode(' ', $line);
                $status = intval($lineParts[1]);
                $header = true;
            }
            else if ($line == '')
            {
                $header = false;
            }
            else if ($header)
            {
                $line = explode(': ', $line);
                if ($line[0] == 'Status')
                {
                    $status = intval(substr($line[1], 0, 3));
                }
            }
            else
            {
                $content[] = $line;
            }
        }

        if ($status !== $expectedHttpCode)
        {
            throw new SignthisException("Expected status [{$expectedHttpCode}], actual status [{$status}], URL [{$url}], Response [{$response}]", SignthisException::INVALID_HTTP_CODE);
        }

        $object = json_decode(implode("\n", $content));

        return $object;
    }
}
