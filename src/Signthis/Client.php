<?php

namespace Signthis;

use Signthis\Services\Auth;
use Signthis\Services\Documents;

class Client extends ClientBase
{
    public $auth;
    public $documents;

    public function __construct()
    {
        $this->auth = new Auth($this);
        $this->documents = new Documents($this);
    }
}
