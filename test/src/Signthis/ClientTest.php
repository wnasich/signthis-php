<?php
namespace Signthis\Test;

use Signthis\Client;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $this->client = new Client();
        $this->client->setApiKey('my_token');
        $this->client->setTimeout(480);
    }

    /**
     * Test loading with a good API connection.
     * @covers \Signthis\Client::setApiKey
     * @covers \Signthis\Client::setTimeout
     * @test
     */
    public function testConfigure()
    {
        $reflection = new \ReflectionClass('Signthis\Client');

        $apiKey = $reflection->getProperty('apiKey');
        $apiKey->setAccessible(true);
        $this->assertEquals('my_token', $apiKey->getValue($this->client));

        $timeout = $reflection->getProperty('timeout');
        $timeout->setAccessible(true);
        $this->assertEquals(480, $timeout->getValue($this->client));
    }
}
