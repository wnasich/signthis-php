<?php
namespace Signthis\Services\Test;

use Signthis\Client;
use Signthis\Services\Documents;

class DocumentsTest extends \PHPUnit_Framework_TestCase
{
    function setUp()
    {
        $this->clientMock = $this->getMockBuilder('Signthis\Client')
            ->setMethods(array('request'))
            ->getMock();
    }

    /**
     * Test all()
     * @covers \Signthis\Services\Documents::all
     * @test
     */
    public function testAll()
    {
        $data = array();

        $this->clientMock->expects($this->once())
            ->method('request')
            ->with('documents.json', 'GET', $data);

        $documents = new Documents($this->clientMock);
        $documents->all();
    }

    /**
     * Test find($documentId)
     * @covers \Signthis\Services\Documents::find
     * @test
     */
    public function testFind()
    {
        $documentId = '65297ef1-2b9f-4f84-9a4f-f8062335c51a';
        $data = array();

        $this->clientMock->expects($this->once())
            ->method('request')
            ->with("documents/$documentId.json", 'GET', $data);

        $documents = new Documents($this->clientMock);
        $documents->find($documentId);
    }

    /**
     * Test create($signers, $emailBody, $documentName, $fileData)
     * @covers \Signthis\Services\Documents::create
     * @test
     */
    public function testCreate()
    {
        $signers = array(
            array('first_name' => 'John', 'last_name' => 'Doe', 'email' => 'john.doe@example.com'),
            array('first_name' => 'Jane', 'last_name' => 'Barbera', 'email' => 'jane.barbera@example.com'),
            array('first_name' => 'Mat', 'last_name' => 'Smith', 'email' => 'mat.smith@example.com')
        );

        $emailBody = 'A new document is waiting for you';
        $documentName = 'Document001';
        $fileData = 'binary_data';

        $data = array('document' => array(
            'document_name' => $documentName,
            'document' => 'data:application/pdf;base64,' . base64_encode($fileData),
            'email_body' => $emailBody,
            'signers_attributes' => $signers,
        ));

        $this->clientMock->expects($this->once())
            ->method('request')
            ->with("documents.json", 'POST', $data, 201);

        $documents = new Documents($this->clientMock);
        $documents->create($signers, $emailBody, $documentName, $fileData);
    }

    /**
     * Test cancel($documentId)
     * @covers \Signthis\Services\Documents::cancel
     * @test
     */
    public function testCancel()
    {
        $documentId = '65297ef1-2b9f-4f84-9a4f-f8062335c51a';

        $this->clientMock->expects($this->once())
            ->method('request')
            ->with("documents/$documentId/cancel.json", 'DELETE');

        $documents = new Documents($this->clientMock);
        $documents->cancel($documentId);
    }

    /**
     * Test addDragSigner($documentId, $signerId, $page, $top, $left)
     * @covers \Signthis\Services\Documents::addDragSigner
     * @test
     */
    public function testAddDragSigner()
    {
        $documentId = '65297ef1-2b9f-4f84-9a4f-f8062335c51a';
        $signerId = '123456';
        $page = 1;
        $top = 100;
        $left = 200;

        $data = array('drag_signer' => array(
            'page' => $page,
            'top' => $top,
            'left' => $left,
        ));

        $this->clientMock->expects($this->once())
            ->method('request')
            ->with("documents/$documentId/signers/$signerId/drag_signers.json", 'POST', $data, 201);

        $documents = new Documents($this->clientMock);
        $documents->addDragSigner($documentId, $signerId, $page, $top, $left);
    }

    /**
     * Test donePlaceholders($documentId)
     * @covers \Signthis\Services\Documents::donePlaceholders
     * @test
     */
    public function testDonePlaceHolders()
    {
        $documentId = '65297ef1-2b9f-4f84-9a4f-f8062335c51a';

        $this->clientMock->expects($this->once())
            ->method('request')
            ->with("documents/$documentId/done_placeholders.json", 'PUT', array(), 204);

        $documents = new Documents($this->clientMock);
        $documents->donePlaceholders($documentId);
    }

    /**
     * Test send($documentId, $isPaymentWithSignature)
     * @covers \Signthis\Services\Documents::send
     * @test
     */
    public function testSend()
    {
        $documentId = '65297ef1-2b9f-4f84-9a4f-f8062335c51a';
        $data = array('payment' => array(
            'is_payment_with_signature' => 'false',
        ));

        $this->clientMock->expects($this->once())
            ->method('request')
            ->with("documents/$documentId/finish_template.json", 'PUT', $data, 204);

        $documents = new Documents($this->clientMock);
        $documents->send($documentId);
    }
}
