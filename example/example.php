<?php
require_once('../vendor/autoload.php');
use Signthis\Client;

$client = new Client();

$auth = $client->auth->login('email', 'password');
$client->setApiKey($auth->authentication_token);

// $client->setApiKey('your_token');

$signers = array(
	array('first_name' => 'FirstName1', 'last_name' => 'LastName1', 'email' => 'signthisfirstname1@testdomain.com'),
	array('first_name' => 'FirstName2', 'last_name' => 'LastName1', 'email' => 'signthisfirstname2@testdomain.com'),
);
$emailBody = 'Please sign this doc';
$documentName = 'Document 001';
$data = file_get_contents('/path/to/your/document.pdf');
$document = $client->documents->create($signers, $emailBody, $documentName, $data);

$places = array(
	array('top' => 500, 'left' => 500),
	array('top' => 550, 'left' => 450),
);
foreach ($document->signers as $place => $signer) {
	$dragSigner = $client->documents->addDragSigner($document->id, $signer->id, 0, $places[$place]['top'], $places[$place]['left']);
}

$donePlaceholders = $client->documents->donePlaceholders($document->id);

$send = $client->documents->send($document->id);

$docs = $client->documents->find($document->id);
var_dump($docs);

$logout = $client->auth->logout();
