# Signthis PHP Client

This is the PHP wrapper for [Signthis REST API](http://signthis.ca/).

[ ![Codeship Status for wnasich/signthis-php](https://codeship.com/projects/57ebeaf0-49d8-0134-b264-16ec256c4313/status?branch=master)](https://codeship.com/projects/169572)


## Setup

You must provide a valid `email` and `password` in order to use the library.

```php
require_once(__DIR__ . "/signthis/SignthisClient.php");

$client = new Client();
$client->auth->login('email', 'password');
```

see example file [example/example.php](https://bitbucket.org/wnasich/signthis-php/src/master/example/example.php?at=master)